"use client";
import React, { useState } from "react";
import axios from "axios";

const BackUpload = () => {
 const [file, setFile] = useState<any>(null);

 const handleFileChange = (e: any) => {
  setFile(e.target.files[0]);
 };

 const handleUpload = async () => {
  try {
   const formData = new FormData();

   formData.append("excelFile", file);

   const response = await axios.post("http://localhost:5000/batch", formData, {
    method: "POST",
    headers: {
     "Content-Type": "multipart/form-data",
    },
   });

   console.log(response.data);
  } catch (error) {
   console.error("Error uploading file:", error);
  }
 };

 return (
  <div className="App">
   <h1>Excel Upload</h1>
   <input
    type="file"
    accept=".xlsx"
    onChange={handleFileChange}
   />
   <button
    onClick={handleUpload}
    disabled={!file}
   >
    Upload
   </button>
  </div>
 );
};

export default BackUpload;
