/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import React, { useEffect, useRef, useState } from "react";
import io from "socket.io-client";
import { useParams } from "next/navigation";
// import { ReactMediaRecorder } from "react-media-recorder";
// import Peer from "peerjs";

import { useReactMediaRecorder } from "react-media-recorder";
import Link from "next/link";

type UserInfoType = {
 name: String;
};

const socket = io("https://socketio.1clickdesign.com");

const SocketComponents: React.FC<UserInfoType> = ({ name }) => {
 const videoGridRef = useRef<any>(null);
 const myVideoRef = useRef<any>(null);
 const peersRef = useRef<any>({});
 const screenShareRef = useRef<any>({});
 const [message, setMessage] = useState<string>("");
 const [messages, setMessages] = useState<string[]>([]);

 let myPeerRef: any;

 const route = useParams();
 const ROOM_ID = route.id;

 const [username, setUsername] = useState("");

 const { status, startRecording, stopRecording, mediaBlobUrl } =
  useReactMediaRecorder({ screen: true });

 useEffect(() => {
  import("peerjs").then(({ default: Peer }) => {
   const videoGrid = videoGridRef.current;
   const myVideo = myVideoRef.current;
   const randomUserId = Math.floor(Math.random() * 10000);

   myPeerRef = new Peer(randomUserId.toString(), {
    host: "peerjs.1clickdesign.com",
    secure: true,
    config: {
     iceServers: [
      { urls: "stun:stun.relay.metered.ca:80" },
      {
       urls: "turn:global.relay.metered.ca:80",
       username: "1619cdadd226bfda6b6ab4c1",
       credential: "suwODwaDdRVT9EVY",
      },
      {
       urls: "turn:global.relay.metered.ca:80?transport=tcp",
       username: "1619cdadd226bfda6b6ab4c1",
       credential: "suwODwaDdRVT9EVY",
      },
      {
       urls: "turn:global.relay.metered.ca:443",
       username: "1619cdadd226bfda6b6ab4c1",
       credential: "suwODwaDdRVT9EVY",
      },
      {
       urls: "turns:global.relay.metered.ca:443?transport=tcp",
       username: "1619cdadd226bfda6b6ab4c1",
       credential: "suwODwaDdRVT9EVY",
      },
     ],
    },
   });

   myPeerRef.on("open", (id: any) => {
    socket.emit("join-room", ROOM_ID, id);
   });

   navigator.mediaDevices
    .getUserMedia({
     video: true,
     audio: true,
    })
    .then((stream: any) => {
     // current user camera
     addVideoStream(myVideo, stream, randomUserId);

     //  2nd User/Joiner
     myPeerRef.on("call", (call: any) => {
      call.answer(stream);
      const video = document.createElement("video");

      call.on("stream", (userVideoStream: any) => {
       addVideoStream(video, userVideoStream, call.peer);
      });
     });

     socket.on("user-connected", (userId: any) => {
      // setUsername(name.name);
      connectToNewUser(userId, stream);
     });
    });

   socket.on("user-disconnected", (userId: any) => {
    if (peersRef) {
     const videoToRemove = document.getElementById(userId);

     setUsername("");

     if (videoToRemove) {
      videoToRemove.remove();
     }

     if (peersRef.current[userId]) {
      console.log(userId);
      peersRef.current[userId].close();
     }
     delete peersRef.current[userId];
    }
   });

   return () => {
    socket.off("join-room");
    socket.off("user-connected");
    socket.disconnect();
    myPeerRef.destroy();
   };
  });
 }, []);

 function connectToNewUser(userId: any, stream: any) {
  const call = myPeerRef.call(userId, stream);
  const video = document.createElement("video");

  video.setAttribute("id", userId);

  call.on("stream", (userVideoStream: any) => {
   addVideoStream(video, userVideoStream, userId);
  });

  call.on("close", () => {
   video.remove();
  });

  peersRef.current[userId] = call;
 }

 function addVideoStream(video: any, stream: any, userId?: any) {
  video.srcObject = stream;

  video.addEventListener("loadedmetadata", () => {
   video.play();
  });

  video.setAttribute("id", userId);

  // const nametag = document.createElement("p");
  // const remoteName = document.createTextNode(userId);

  // nametag.appendChild(remoteName);

  // const wrapper = document.createElement("div");
  // wrapper.setAttribute("id", userId);
  // wrapper.appendChild(nametag);

  videoGridRef.current.append(video);

  // videoGridRef.current.appendChild(wrapper);
 }

 const toggleCamera = () => {
  if (myVideoRef.current) {
   const videoTrack = myVideoRef.current.srcObject.getVideoTracks()[0];
   videoTrack.enabled = !videoTrack.enabled;
   return videoTrack.enabled;
  }
 };

 const toggleMic = () => {
  if (myVideoRef.current) {
   const audioTrack = myVideoRef.current.srcObject.getAudioTracks()[0];
   audioTrack.enabled = !audioTrack.enabled;
   return audioTrack.enabled;
  }
 };

 //  const shareScreen = () => {
 //   navigator.mediaDevices.getDisplayMedia({ cursor: true }).then((stream) => {
 //    const videoTrack = stream.getVideoTracks()[0];
 //    videoTrack.onended = () => {
 //     shareScreen();
 //    };

 //    myPeerRef.current.srcObject = stream;
 //   });

 //   return;
 //  };

 const sendMessage = () => {
  console.log(message);
  setMessages((prev) => [...prev, message]);
  socket.emit("send-message", message);
  setMessage("");
  return;
 };

 useEffect(() => {
  socket.on("receive-message", (messagereceived: string) => {
   setMessages((prev) => [...prev, messagereceived]);
  });
 }, []);

 return (
  <div>
   <div>
    <p>{status}</p>
    <button onClick={startRecording}>Start Recording</button>
    <button onClick={stopRecording}>Stop Recording</button>
    <video
     src={mediaBlobUrl}
     controls
     autoPlay
     loop
     className="max-w-md"
    />

    <a
     href={mediaBlobUrl}
     target="_blank"
     rel="noreferrer"
     download={mediaBlobUrl}
    >
     Download
    </a>
   </div>
   <div>
    <button onClick={toggleCamera}>Camera on/off</button>
    <button onClick={toggleMic}>Mic on/off</button>
    {/* <button onClick={shareScreen}>Share screen on/off</button> */}
   </div>
   <div>
    <input
     type="text"
     value={message}
     onChange={(e) => setMessage(e.target.value)}
    />
    <button onClick={sendMessage}>Send Messsage</button>
    <ul>
     {messages.map((item, index) => (
      <li key={index}>{item}</li>
     ))}
    </ul>
   </div>
   <Link href="/">Home</Link>
   <div className="video_conf overflow-y-scroll h-[95vh] pe-3">
    <video
     ref={myVideoRef}
     muted
     playsInline
    ></video>
    <div ref={videoGridRef}></div>
    {/* <div>{username}</div> */}
    <video ref={screenShareRef}></video>
   </div>
  </div>
 );
};

export default SocketComponents;
