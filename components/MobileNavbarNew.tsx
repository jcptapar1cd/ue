"use client";
import React, { useState } from "react";

const MobileNavbarNew = () => {
 const [isNavOpen, setIsNavOpen] = useState(false);

 const toggleNav = () => {
  setIsNavOpen(!isNavOpen);
 };

 return (
  <>
   <nav className="bg-blue-500 h-10">
    <div
     className="hamburger md:hidden relative cursor-pointer right-5% top-1/2 transform -translate-y-1/2 z-2"
     onClick={toggleNav}
    >
     <div className="line w-5 h-0.5 bg-white m-1"></div>
     <div className="line w-5 h-0.5 bg-white m-1"></div>
     <div className="line w-5 h-0.5 bg-white m-1"></div>
    </div>
    <ul
     className={`nav-links md:flex list-none w-1/2 h-full justify-around items-center ml-auto ${
      isNavOpen
       ? "fixed inset-0 bg-blue-500 h-full w-full overflow-hidden"
       : "hidden"
     }`}
    >
     <li>
      <a
       href="/23"
       className="text-white text-base"
      >
       About
      </a>
     </li>
     <li>
      <a
       href="/33"
       className="text-white text-base"
      >
       Contact
      </a>
     </li>
     <li>
      <a
       href="/44"
       className="text-white text-base"
      >
       Projects
      </a>
     </li>
    </ul>
   </nav>

   <div className="md:hidden fixed inset-0 bg-blue-500 h-full w-full overflow-hidden nav-links">
    <div
     className="hamburger absolute cursor-pointer right-5% top-1/2 transform -translate-y-1/2 z-2"
     onClick={toggleNav}
    >
     <div className="line w-5 h-0.5 bg-white m-1"></div>
     <div className="line w-5 h-0.5 bg-white m-1"></div>
     <div className="line w-5 h-0.5 bg-white m-1"></div>
    </div>
    <ul className="flex flex-col h-full w-full justify-center items-center">
     <li className="fade opacity-0 transition-all ease-in-out duration-500 delay-200">
      <a
       href="/23"
       className="text-white text-2xl"
      >
       About
      </a>
     </li>
     <li className="fade opacity-0 transition-all ease-in-out duration-500 delay-400">
      <a
       href="/33"
       className="text-white text-2xl"
      >
       Contact
      </a>
     </li>
     <li className="fade opacity-0 transition-all ease-in-out duration-500 delay-600">
      <a
       href="44"
       className="text-white text-2xl"
      >
       Projects
      </a>
     </li>
    </ul>
   </div>

   <section className="landing flex justify-center items-center flex-col h-90vh">
    <h1 className="mt-16 text-5xl text-purple-600">Dots</h1>
   </section>

   {/* Your other components or content go here */}

   {/* <script src="app.js"></script> */}
  </>
 );
};

export default MobileNavbarNew;
